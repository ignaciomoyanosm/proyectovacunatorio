
public abstract class Vacuna {
	private String nombre;
	protected Fecha fechaIngreso;

	abstract boolean ConsultarEsSoloParaMayores60();
	abstract int ConsultarTemperaturaAGuardar();
	public Vacuna(String nombre, Fecha fechaIngreso) {
		this.nombre=nombre;
		this.fechaIngreso=fechaIngreso;
	}
	public Fecha getFechaIngreso() {
		return fechaIngreso;
	}
	public void setFechaIngreso(Fecha fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}
	public String ConsultarNombre() {
		return nombre;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String toString() {
		return "Vacuna: " + this.ConsultarNombre();
	}
}


