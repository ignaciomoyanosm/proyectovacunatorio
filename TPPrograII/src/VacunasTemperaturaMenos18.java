public abstract class VacunasTemperaturaMenos18 extends Vacuna {
	public VacunasTemperaturaMenos18(String nombre, Fecha fechaIngreso) {
		super(nombre, fechaIngreso);
		// TODO Auto-generated constructor stub
	}
	abstract boolean EstaVencida();
	private int temperatura;
	@Override
	public int ConsultarTemperaturaAGuardar() {
		return -18;
	}
	public int getTemperatura() {
		return temperatura;
	}
	public void setTemperatura(int temperatura) {
		this.temperatura = temperatura;
	}
	

}
