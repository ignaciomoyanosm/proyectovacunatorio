import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class CentroVacunacion {
	private String nombreCentro;
	private int capacidadDiariaVacunacion;
	private HashMap <Vacuna, Integer> depositoVacunas;
	private ArrayList <Persona> listaDeEspera;
	private ArrayList <Turno> agendaTurnos;
	private HashMap <Persona, Vacuna> Vacunados;
	private HashMap<Integer, String> Reporte;
	private HashMap<String, Integer> vacunasVencidas;
	public CentroVacunacion(String nombreCentro,int capacidadDiariaVacunacion) {
		if (capacidadDiariaVacunacion <= 0) {
			throw new RuntimeException("La capacidad del Centro debe ser positiva");
		}
		else { 
			if (nombreCentro.length()==0) {
			throw new RuntimeException("El nombre del centro no esta definido");
			}
			this.nombreCentro=nombreCentro;
			this.capacidadDiariaVacunacion=capacidadDiariaVacunacion;
			depositoVacunas = new HashMap <Vacuna, Integer>();
			listaDeEspera = new ArrayList <Persona>();
			agendaTurnos = new ArrayList <Turno>();
			Vacunados = new HashMap<Persona, Vacuna>();
			Reporte = new HashMap<Integer, String>();
			vacunasVencidas= new HashMap<String,Integer>();
			vacunasVencidas.put("Pfizer", 0);
		}
	}
	public void ingresarVacunas(String nombreVacuna, int cantidad, Fecha fechaIngreso) {
		Vacuna vacuna = crearVacunaCorrespondiente(nombreVacuna, fechaIngreso);
		if (cantidad <= 0) 
			throw new RuntimeException("La cantidad de vacunas debe ser mayor a 0");
		if  (vacunaRepetida(vacuna))
			modificarCantidad((vacuna), cantidad);
		depositoVacunas.put(vacuna,cantidad);
	}
	public int vacunasDisponibles() {
		 int cantidadVacunas=0;
		for (Map.Entry<Vacuna,Integer> s : depositoVacunas.entrySet()) {
			if(!vacunaVencida(s.getKey())) 
				cantidadVacunas+=s.getValue();
		}
		return cantidadVacunas;
	}
	public int vacunasDisponibles(String nombreVacuna) {
		int cantidadVacunas=0;
		for(Map.Entry<Vacuna, Integer> s : depositoVacunas.entrySet()) {
			if (!vacunaVencida(s.getKey())&&(s.getKey().ConsultarNombre().equals(nombreVacuna))){
				cantidadVacunas+=s.getValue();
			}
		}
		return cantidadVacunas;
	}
	public void inscribirPersona(int dni, Fecha nacimiento,boolean tienePadecimientos, boolean esEmpleadoSalud) {
		if (!esMayorDeEdad(nacimiento)) {
			throw new RuntimeException("Se debe ser mayor de 18 aņos para vacunarse");
		}
		if (seEncuentraInscripta(dni)) {
			throw new RuntimeException("La persona ya se encuentra inscripta");
		}
		if (estaVacunada(dni)) {
			throw new RuntimeException("La persona ya ha sido vacunada");
		}
		Persona persona = new Persona (dni, nacimiento, tienePadecimientos, esEmpleadoSalud);
		this.listaDeEspera.add(persona);
	}
	public List<Integer> listaDeEspera()
    {
       ArrayList<Integer> listaDniInscriptos = new ArrayList<Integer>();
       for (int i = 0; i < listaDeEspera.size(); i++) {
            listaDniInscriptos.add(listaDeEspera.get(i).ConsultarDNI());
        }
        return listaDniInscriptos;
    }
	public void generarTurnos(Fecha fechaInicial) {
	if (fechaInicial.anterior(Fecha.hoy()))
		throw new RuntimeException("Le fecha indicada para generar Turnos ya paso.");
	turnosVencidos(fechaInicial);
	borrarVacunasVencidas();
	Fecha fechaTurno = new Fecha(fechaInicial);
	asignarTurno(obtenerPersonaPrioridad(3),fechaTurno);
	asignarTurno(obtenerPersonaPrioridad(2),fechaTurno);
	asignarTurno(obtenerPersonaPrioridad(1),fechaTurno);
	asignarTurno(obtenerPersonaPrioridad(0),fechaTurno);
	
	}
	
	
	public List<Integer> turnosConFecha(Fecha fecha){
		ArrayList<Integer> listaDNIturnosConFecha = new ArrayList<Integer>();
		for (int i=0; i<agendaTurnos.size(); i++) {
			if (agendaTurnos.get(i).getFecha().equals(fecha)) { 
				//System.out.println(agendaTurnos.get(i).toString());
				listaDNIturnosConFecha.add(agendaTurnos.get(i).getPersona().ConsultarDNI());
			}
		}
		return listaDNIturnosConFecha;
	}
	public void vacunarInscripto(int dni, Fecha fechaVacunacion) {
		Turno turnoInscripto = (buscarTurno(dni));
		if(turnoInscripto==null) {
			throw new RuntimeException("La persona no esta inscripta");
		}
		if(!turnoInscripto.getFecha().equals(fechaVacunacion)) {
			throw new RuntimeException("La persona no tiene turno hoy");
		}
		agregarVacunados(turnoInscripto);
		//borrarVacuna(turnoInscripto);
				
	}
	public Map<Integer, String> reporteVacunacion(){
		for(Map.Entry<Persona, Vacuna> s : Vacunados.entrySet())
			Reporte.put(s.getKey().ConsultarDNI(), s.getValue().ConsultarNombre());
		return Reporte;
	}
	public Map<String, Integer> reporteVacunasVencidas(){
		return  vacunasVencidas;
	}
	public int TamaņoAgenda() {
		return agendaTurnos.size();
	}
	public int TamaņoVacunados() {
		return Vacunados.size();
	}
	public int TamaņoVacunasVencidas() {
		return vacunasVencidas.size();	
		}
	
	private boolean estaVacunada (int dni) {
		boolean estaVacunada=false;
		for(Map.Entry<Persona, Vacuna> s : Vacunados.entrySet())
			estaVacunada = estaVacunada || s.getKey().ConsultarDNI()==(dni);
		return estaVacunada;
			
	}
	private Vacuna crearVacunaCorrespondiente(String nombreVacuna, Fecha fechaingreso) {
		if(nombreVacuna.equalsIgnoreCase("Sputnik")) {
			return new Sputnik(nombreVacuna,fechaingreso);
		}
		if(nombreVacuna.equalsIgnoreCase("Astrazeneca")) {
			return new Astrazeneca(nombreVacuna,fechaingreso);
		}
		if(nombreVacuna.equalsIgnoreCase("Pfizer")) {
			return new Pfizer(nombreVacuna,fechaingreso);
		}
		if(nombreVacuna.equalsIgnoreCase("Moderna")) {
			return new Moderna(nombreVacuna,fechaingreso);
		}
		if(nombreVacuna.equalsIgnoreCase("Sinopharm")) {
			return new Sinopharm(nombreVacuna,fechaingreso);
		}
		else
			throw new RuntimeException("La vacuna ingresada no corresponde con ninguna de las disponibles");
	}
	private boolean vacunaRepetida(Vacuna vacuna) {
		boolean aux=false;
		for(Map.Entry<Vacuna, Integer> s : depositoVacunas.entrySet()) {
			if (vacuna.equals(s.getKey())) {
				aux = aux || true;}
		}
		return aux;
	}
	private boolean vacunaVencida(Vacuna vacuna){
		if (vacuna instanceof VacunasTemperaturaMenos18) {
			VacunasTemperaturaMenos18 ej = (VacunasTemperaturaMenos18) vacuna;
			return ej.EstaVencida();
		} 
		else 
			return false;
	}
	private boolean esMayorDeEdad(Fecha nacimiento) {
		if(Fecha.diferenciaAnios(Fecha.hoy(), nacimiento)>18)
			return true;
		else 
			return false;
	}
	private boolean seEncuentraInscripta(int dni) {
		boolean aux = false;
		for (int i=0; i<listaDeEspera.size();i++) {
			aux = aux || listaDeEspera.get(i).ConsultarDNI() == dni;
		}
		return aux;
	}
	private void agregarVacunados(Turno turno) {
		Vacunados.put(turno.getPersona(), turno.getVacuna());
		agendaTurnos.remove(turno);
	}
	private void reservarVacuna(Turno turno) {
		for(Map.Entry<Vacuna, Integer> s : depositoVacunas.entrySet())
			if(turno.getVacuna().equals(s.getKey())) {
				if(s.getValue()>1)
					depositoVacunas.replace(turno.getVacuna(),s.getValue()-1);
				else
					depositoVacunas.remove(turno.getVacuna());
			}
	}
	private void borrarVacunasVencidas() {
		for(Map.Entry<Vacuna, Integer> s : depositoVacunas.entrySet()) {
			if(vacunaVencida(s.getKey())){
					System.out.println(s.getKey().toString());
					sumarVacunaVencidas(s.getKey(),s.getValue());
					System.out.println(vacunasVencidas.size());
					//depositoVacunas.remove(s.getKey());
			}
		}
		System.out.println("a"+vacunasVencidas.size());
	}
	private void sumarVacunaVencidas(Vacuna vacuna, int cantidad) {
	
		for(Map.Entry<String, Integer> s : vacunasVencidas.entrySet()) {
			if (s.getKey().equalsIgnoreCase(vacuna.ConsultarNombre())) {
				vacunasVencidas.replace(s.getKey(), s.getValue()+cantidad);}
		}
	}

	private void modificarCantidad(Vacuna vacuna, int cantidad) {
		for(Map.Entry<Vacuna, Integer> s : depositoVacunas.entrySet()) {
			if(vacuna.equals(s.getKey())) {
				depositoVacunas.replace(vacuna, s.getValue()+cantidad);
			}
		}
	}
	private Turno buscarTurno (int dni) {
		for (int i=0; i<agendaTurnos.size();i++) {
			if (agendaTurnos.get(i).getPersona().ConsultarDNI()==dni)
				return agendaTurnos.get(i);
			
		}
		return null;
	}
	private Vacuna buscarVacuna(String nombre) {
		for(Map.Entry<Vacuna, Integer> s : depositoVacunas.entrySet()) {
			if(s.getKey().ConsultarNombre().equalsIgnoreCase(nombre)) 
				return s.getKey();
		}
		return null;
	}

	private int edad(Persona persona) {
		return Fecha.diferenciaAnios(Fecha.hoy(), persona.getnacimiento());
	}
	private void quitarPersonaLista(Turno turno) {
		listaDeEspera.remove(buscarPersona(turno.getPersona().ConsultarDNI()));

	}
	
	private Persona buscarPersona(int dni) {
		for (int i=0; i<listaDeEspera.size(); i++) {
			if (listaDeEspera.get(i).ConsultarDNI()==dni)
				return listaDeEspera.get(i);
		}
		return null;
	}
	private int calcularPrioridadPersona(Persona persona) {
		if(persona.ConsultarTrabajaSalud()==true) 
			return 3;
		if(edad(persona)>60) 
			return 2;
		if(persona.ConsultarEnfermedadesDeRiesgo()==true) 
			return 1;
		
		return 0;
	}
	private ArrayList<Persona> obtenerPersonaPrioridad(int prio) {
		ArrayList<Persona> PersonasConPrioridad = new ArrayList<Persona>();
		for (int i=0; i<listaDeEspera.size();i++) {
			if (calcularPrioridadPersona(listaDeEspera.get(i))==prio)
				PersonasConPrioridad.add(listaDeEspera.get(i));
		}
		return PersonasConPrioridad;
	}
	private void asignarTurno(ArrayList <Persona> listaPersonasTurno, Fecha fechaTurno) {
		for (int i =0; i<listaPersonasTurno.size();i++) {
			/*System.out.println(fechaTurno.toString());
			System.out.println(turnosConFecha(fechaTurno).size());
			System.out.println(this.capacidadDiariaVacunacion);*/
			if(turnosConFecha(fechaTurno).size()==this.capacidadDiariaVacunacion) 
				fechaTurno.avanzarUnDia();
			if(edad(listaPersonasTurno.get(i))>=60) 
				asignarTurnoMas60(listaPersonasTurno.get(i),fechaTurno);
			if(edad(listaPersonasTurno.get(i))<60)
				asignarTurnoMenos60(listaPersonasTurno.get(i),fechaTurno);
		}
	}
	private void asignarTurnoMas60(Persona persona, Fecha fechaInicial) {
		Fecha fechaAux = new Fecha(fechaInicial);
		for(Map.Entry<Vacuna, Integer> s : depositoVacunas.entrySet()) {
			if (s.getKey().ConsultarEsSoloParaMayores60()==true) {
				Turno turno = new Turno(persona,buscarVacuna(s.getKey().ConsultarNombre()) , fechaAux);
				reservarVacuna(turno);
				quitarPersonaLista(turno);
				agendaTurnos.add(turno);
				return;
			}
			else {
				Turno turno = new Turno(persona,buscarVacuna(s.getKey().ConsultarNombre()) , fechaAux);
				reservarVacuna(turno);
				quitarPersonaLista(turno);
				agendaTurnos.add(turno);
				return;
			}
	}
	}
	private void asignarTurnoMenos60(Persona persona, Fecha fechaInicial) {
		Fecha fechaAux = new Fecha(fechaInicial);
		for(Map.Entry<Vacuna, Integer> s : depositoVacunas.entrySet()) {
			if(s.getKey().ConsultarEsSoloParaMayores60()==false) {
				Turno turno = new Turno(persona,buscarVacuna(s.getKey().ConsultarNombre()) , fechaAux);
				reservarVacuna(turno);
				quitarPersonaLista(turno);
				agendaTurnos.add(turno);
				return;
			}
		}
	}
	private void turnosVencidos(Fecha fechaInicial){
		Fecha fechaAux = new Fecha(fechaInicial);
		
		/*for (int i = 0; i < agendaTurnos.size(); i++) {
			System.out.println(agendaTurnos.get(i).getFecha().toString());
			if(agendaTurnos.get(i).getFecha().anterior(fechaAux)){
				ingresarVacunas(agendaTurnos.get(i).getVacuna().ConsultarNombre(), 1, agendaTurnos.get(i).getVacuna().getFechaIngreso());
				agendaTurnos.remove(i);
			}
		}*/
		Iterator<Turno> it = agendaTurnos.iterator();
		while (it.hasNext()) {
			Turno turno=it.next();
			if(turno.getFecha().anterior(fechaAux)) {
				ingresarVacunas(turno.getVacuna().ConsultarNombre(),1,turno.getVacuna().getFechaIngreso());
				it.remove();
			}
			
		}

	}
}
