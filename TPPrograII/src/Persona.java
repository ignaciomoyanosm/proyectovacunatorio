
public class Persona {
	private Fecha nacimiento;
	private int DNI;
	private boolean TrabajaSalud;
	private boolean EnfermedadesDeRiesgo;
	private boolean EstaVacunado;
	public Persona(int DNI, Fecha nacimiento, boolean TrabajaSalud, boolean EnfermedadesDeRiesgo) {
		this.nacimiento=nacimiento;
		this.DNI=DNI;
		this.TrabajaSalud=TrabajaSalud;
		this.EnfermedadesDeRiesgo=EnfermedadesDeRiesgo;
	}
	public Fecha ConsultarEdad(){
		return this.nacimiento;
	}
	public int ConsultarDNI(){
		return this.DNI;
	}
	public boolean ConsultarTrabajaSalud(){
		return this.TrabajaSalud;
	}
	public boolean ConsultarEnfermedadesDeRiesgo(){
		return this.EnfermedadesDeRiesgo;
	}
	public boolean ConsultarEstaVacunado(){
		return this.EstaVacunado;
	}
	public Fecha getnacimiento() {
		return nacimiento;
	}
	public void setnacimiento(Fecha nacimiento) {
		this.nacimiento = nacimiento;
	}
	public int getDNI() {
		return DNI;
	}
	public void setDNI(int dNI) {
		DNI = dNI;
	}
	public boolean isTrabajaSalud() {
		return TrabajaSalud;
	}
	public void setTrabajaSalud(boolean trabajaSalud) {
		TrabajaSalud = trabajaSalud;
	}
	public boolean isEnfermedadesDeRiesgo() {
		return EnfermedadesDeRiesgo;
	}
	public void setEnfermedadesDeRiesgo(boolean enfermedadesDeRiesgo) {
		EnfermedadesDeRiesgo = enfermedadesDeRiesgo;
	}
	public boolean isEstaVacunado() {
		return EstaVacunado;
	}
	public void setEstaVacunado(boolean estaVacunado) {
		EstaVacunado = estaVacunado;
	}

}
