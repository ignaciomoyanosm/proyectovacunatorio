
public class Moderna extends VacunasTemperaturaMenos18{
	public Moderna(String nombre, Fecha fechaIngreso) {
		super(nombre, fechaIngreso);
		// TODO Auto-generated constructor stub
	}
	private boolean esSoloParaMayoresDe60;
	@Override
	public boolean EstaVencida(){
		if(Fecha.diferenciaAnios(Fecha.hoy(),fechaIngreso)>60){
			return true;
		}
		return false;
	}
	public boolean ConsultarEsSoloParaMayores60() {
		return false;
	}
	public boolean isEsSoloParaMayoresDe60() {
		return esSoloParaMayoresDe60;
	}
	public void setEsSoloParaMayoresDe60(boolean esSoloParaMayoresDe60) {
		this.esSoloParaMayoresDe60 = esSoloParaMayoresDe60;
	}
		

}
