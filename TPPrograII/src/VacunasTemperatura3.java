  
public abstract class VacunasTemperatura3 extends Vacuna {
	public VacunasTemperatura3(String nombre, Fecha fechaIngreso) {
		super(nombre, fechaIngreso);
		// TODO Auto-generated constructor stub
	}
	private int temperatura;
	@Override
	public int ConsultarTemperaturaAGuardar() {
		return 3;
	}
	public int getTemperatura() {
		return temperatura;
	}
	public void setTemperatura(int temperatura) {
		this.temperatura = temperatura;
	}

}
